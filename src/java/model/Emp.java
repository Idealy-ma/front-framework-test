/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import annotations.Url;
import java.util.HashMap;
import util.ModelView;

/**
 *
 * @author i.m.a
 */

public class Emp {
    private String nom;
    private String prenom;
    private int age;

    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    @Url(url="select")
    public ModelView select(){
        ModelView mv = new ModelView();
        
        mv.setUrl("view.jsp");
        
        HashMap<String, Object> value = new HashMap<>();
        value.put("nom", this.getNom());
        value.put("prenom", this.getPrenom());
        value.put("age", this.getAge());
        
        mv.setValues(value);
        
        return mv;
    }
}
