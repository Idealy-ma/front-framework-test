/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import annotations.Url;
import java.util.HashMap;
import util.ModelView;

/**
 *
 * @author i.m.a
 */
public class Emp {
    private String nom;
    private String prenom;
    private int age;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    @Url(url="emp-info")
    public ModelView info(){
        /***
         * Test sprint 1 a 3
         */
        
        // Creation d'un model view
        ModelView mv = new ModelView();
        
        //URL pour le dispatch
        mv.setUrl("view.jsp");
        
        // Creation des valeurs a afficher
        HashMap<String, Object> value = new HashMap<>();
        
        /***
         * Ajout des valeurs a afficher
         * Ici on utilise les attributs
         * La valeur de ces attributs viennent du vue
         */
        value.put("nom", this.getNom());
        value.put("prenom", this.getPrenom());
        value.put("age", this.getAge());

        
        mv.setValues(value);
        
        System.out.println("main.Model.mode()");
        
        return mv;
    }
}
