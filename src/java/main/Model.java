/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import annotations.Url;
import java.util.HashMap;
import util.ModelView;

/**
 *
 * @author i.m.a
 */
public class Model {
    int nombre;
    String chiffre;

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public String getChiffre() {
        return chiffre;
    }

    public void setChiffre(String chiffre) {
        this.chiffre = chiffre;
    }
    
    @Url(url="get-and-send")
    public ModelView mode(){
        /***
         * Test sprint 1 a 3
         */
        
        // Creation d'un model view
        ModelView mv = new ModelView();
        
        //URL pour le dispatch
        mv.setUrl("view.jsp");
        
        // Creation des valeurs a afficher
        HashMap<String, Object> value = new HashMap<>();
        
        /***
         * Ajout des valeurs a afficher
         * Ici on utilise les attributs
         * La valeur de ces attributs viennent du vue
         */
        value.put("nombre", this.getNombre());
        value.put("chiffre", this.getChiffre());
        
        mv.setValues(value);
        
        System.out.println("main.Model.mode()");
        
        return mv;
    }
}
