<%-- 
    Document   : view
    Created on : Nov 9, 2022, 7:45:08 AM
    Author     : i.m.a
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p><b>Nom :</b> <%= request.getAttribute("nom") %></p>
        <p><b>Prenom :</b> <%= request.getAttribute("prenom") %></p>
        <p><b>Age :</b> <%= request.getAttribute("age") %></p>
    </body>
</html>
